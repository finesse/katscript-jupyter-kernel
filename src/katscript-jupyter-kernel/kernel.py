from ipykernel.kernelbase import Kernel
import finesse
import textwrap
import io
import base64
import urllib
import re


def fig_to_svg(fig):
    data = io.StringIO()
    fig.savefig(data, format="svg")
    data.seek(0)
    return data.getvalue()


def fig_to_png(fig):
    png = io.BytesIO()
    fig.savefig(png, format="png")
    png.seek(0)
    return base64.b64encode(png.getvalue()).decode()


class KatScriptKernel(Kernel):
    implementation = "KatScript"
    implementation_version = "1.0"
    language = "KatScript"
    language_version = "3.0"
    language_info = {
        "name": "katscript",
        "mimetype": "text/x-katscript",
        "codemirror_mode": {
            "name": "katscript",
        },
        "pygments_lexer": "katscript",
        "nbconvert_exporter": "python",
        "file_extension": ".kat",
    }
    banner = textwrap.dedent(
        rf"""
        ------------------------------------------------------------------------
                             FINESSE 3.0
                o_.-=.       Frequency domain INterferomEter Simulation SoftwarE
                (\'".\|                         http://www.gwoptics.org/finesse/
                .>' (_--.
            _=/d   ,^\       Jupyter Kernel
            ~~ \)-'   '
            / |
            '  '
        ------------------------------------------------------------------------
        """
    ).strip()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        finesse.init_plotting()
        self.model = finesse.Model()

    def do_execute(
        self, code, silent, store_history=True, user_expressions=None, allow_stdin=False
    ):
        splits = re.split(r"^%reset", code)
        for split in splits[:-1]:
            if split.strip():
                self.do_execute(
                    split, silent, store_history, user_expressions, allow_stdin
                )
            self.model = finesse.Model()
        code = splits[-1]
        try:
            self.model.parse(code)
        except Exception as e:
            stream_content = {"name": "stderr", "text": str(e)}
            self.send_response(self.iopub_socket, "stream", stream_content)

            return {
                "status": "error",
                "execution_count": self.execution_count,
                "payload": [],
                "user_expressions": {},
            }

        if self.model.analysis:

            def try_plot(out):
                if not hasattr(out, "plot"):
                    return
                figures = out.plot(show=False)
                for key, fig in figures.items():
                    if type(key) is str:
                        # Ignore the detector-name keys
                        continue
                    figsize = fig.get_size_inches() * fig.dpi
                    content = {
                        "data": {
                            "image/svg+xml": fig_to_svg(fig),
                            "image/png": fig_to_png(fig),
                        },
                        "metadata": {
                            "image/png": {"width": figsize[0], "height": figsize[1]}
                        },
                    }
                    self.send_response(self.iopub_socket, "display_data", content)

            out = self.model.run()
            try_plot(out)
            for child in out.get_all_children():
                try_plot(child)

        return {
            "status": "ok",
            "execution_count": self.execution_count,
            "payload": [],
            "user_expressions": {},
        }


if __name__ == "__main__":
    from ipykernel.kernelapp import IPKernelApp

    IPKernelApp.launch_instance(kernel_class=KatScriptKernel)
